<?php

namespace znexx;

class Logger {

	private $consoleLogLevel = self::NONE;
	private $fileLogLevel = self::NONE;
	private $filename = '';

	public const DEEP_DEBUG = 0;
	public const DEBUG = 1;
	public const INFO = 2;
	public const WARNING = 3;
	public const ERROR = 4;
	public const NONE = 5;

	private const LEVEL_NAMES = [
		self::DEEP_DEBUG => 'DEEP DEBUG',
		self::DEBUG => 'DEBUG',
		self::INFO => 'INFO',
		self::WARNING => 'WARNING',
		self::ERROR => 'ERROR',
		self::NONE => 'NONE',
	];

	function __construct(int $consoleLogLevel = self::WARNING, int $fileLogLevel = self::NONE, string $filename = '') {
		$this->consoleLogLevel = $consoleLogLevel;
		$this->fileLogLevel = $fileLogLevel;
		$this->filename = $filename;
	}

	/**
	 * Writes the log entry to the console and/or file as appropriate
	 *
	 * If writing to file fails, file logging is deactivated
	 *
	 * @param int $level Is the current log level for this entry
	 * @param string $entry Is the actual text to be written
	 */
	public function writeLogEntry(int $level, string $entry): void {
		if ($this->consoleLogLevel <= $level) {
			echo $entry;
		}

		if ($this->filename && $this->fileLogLevel <= $level) {
			$bytesWritten = file_put_contents($this->filename, $entry, FILE_APPEND | LOCK_EX);
			if ($bytesWritten === false) {
				$this->fileLogLevel = self::NONE;
				$this->error('Failed writing to file ' . $this->filename . '! Deactivating file logging.');
			}
		}
	}

	/**
	 * Formats and recurses (if array) a log entry
	 *
	 * @param int $level Is the current log level for this entry
	 * @param int $backtraceLevel Is the number of entries back in the
	 * 	backtrace we look to find the calling function
	 * @param mixed $message The data to be logged. If this is an array,
	 * 	a recursive call for each entry will be made
	 * @param string $messageKey The array key (if applicable) to be
	 * 	printed with its value
	 */
	private function logMessage(int $level, int $backtraceLevel, $message, $include_args = false): void {
		$backtrace = debug_backtrace();

		//var_dump($backtrace);

		$caller = $backtrace[1];
		$fnStr = basename($caller['file']) . ':' . $caller['line'];

		if (count($backtrace) >= 3) {
			$fnStr .= ' ';
			$fn = $backtrace[2];
			if (array_key_exists('class', $fn)) {
				$fnStr .= $fn['class'] . $fn['type'];
			}
			if (array_key_exists('function', $fn)) {
				if ($include_args === true) {
					$fnStr .= $fn['function'] . json_encode($fn['args']);
				} else {
					$fnStr .= $fn['function'];
				}
			}
		}

		if (is_array($message) || is_object($message)) {
			$s = explode(PHP_EOL, print_r($message, true));
			foreach ($s as $line) {
				$this->writeLogEntry($level, strftime('[%c] ') . self::LEVEL_NAMES[$level] . " $fnStr: $line\n");
			}
		} else {
			$this->writeLogEntry($level, strftime('[%c] ') . self::LEVEL_NAMES[$level] . " $fnStr: $message\n");
		}
		
	}

	/**
	 * Rotates the log file
	 *
	 * Gzips the current log file, appends the unix time to its base name
	 * and .gz to its extension, and empties the current log file.
	 */
	public function rotateLogFile(): void {
		if ($this->filename) {
			if (!file_exists($this->filename)) {
				$this->debug('No log file with filename ' . $this->filename . ' exists. No need to rotate.');
				return;
			}

			$currentLog = file_get_contents($this->filename);
			$gzCurrentLog = gzencode($currentLog, 9);
			$pathInfo = pathinfo($this->filename);

			$dirname = $pathInfo['dirname'] . '/';
			$filename = $pathInfo['filename'] . "_" . time() . '.';
			$extension = $pathInfo['extension'] . '.gz';

			$bytesWritten = file_put_contents($dirname . $filename . $extension, $gzCurrentLog, LOCK_EX);
			if ($bytesWritten === false) {
				$this->error("Could not create gzipped log file! Aborting log rotation");
				return;
			} else {
				$bytesWritten = file_put_contents($this->filename, '', LOCK_EX);
				if ($bytesWritten === false) {
					$this->warning("Could not empty current log file");
				}
			}
		}
	}

	/**
	 * Sets the filename for the log file
	 */
	public function setFilename(string $filename): void {
		$this->filename = $filename;
	}

	/**
	 * Sets the log level for all outputs
	 */
	public function setLogLevel(int $level): void {
		$this->setConsoleLogLevel($level);
		$this->setFileLogLevel($level);
	}

	/**
	 * Sets the log level for the console
	 *
	 * To deactivate the console output, use Logger::NONE
	 */
	public function setConsoleLogLevel(int $level): void {
		$this->consoleLogLevel = $level;
	}

	/**
	 * Gets the log level for the console
	 *
	 * $return int The value for the log level of the console
	 */
	public function getConsoleLogLevel(): int {
		return $this->consoleLogLevel;
	}

	/**
	 * Sets the log level for the log file
	 *
	 * To deactivate the log file output, use Logger::NONE
	 */
	public function setFileLogLevel(int $level): void {
		$this->fileLogLevel = $level;
	}

	/**
	 * Gets the log level for the log file
	 *
	 * $return int The value for the log level of the log file
	 */
	public function getFileLogLevel(): int {
		return $this->fileLogLevel;
	}

	/**
	 * Logs a message with log level Logger::DEEP_DEBUG
	 */
	public function deep_debug($message, $include_arguments = false): void {
		$this->logMessage(self::DEEP_DEBUG, 2, $message, '', $incude_arguments);
	}

	/**
	 * Logs a message with log level Logger::DEBUG
	 */
	public function debug($message, $include_arguments = false): void {
		$this->logMessage(self::DEBUG, 2, $message, '', $include_arguments);
	}

	/**
	 * Logs a message with log level Logger::INFO
	 */
	public function info($message, $include_arguments = false): void {
		$this->logMessage(self::INFO, 2, $message, '', $include_arguments);
	}

	/**
	 * Logs a message with log level Logger::WARNING
	 */
	public function warning($message, $include_arguments = false): void {
		$this->logMessage(self::WARNING, 2, $message, '', $include_arguments);
	}

	/**
	 * Logs a message with log level Logger::ERROR
	 */
	public function error($message, $include_arguments = false): void {
		$this->logMessage(self::ERROR, 2, $message, '', $include_arguments);
	}
}
?>
